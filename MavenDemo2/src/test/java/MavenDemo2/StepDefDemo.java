package MavenDemo2;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StepDefDemo {
	@Given("^My First Step$")
	public void my_First_Step() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		System.out.println("My First Step - Bala");
	}

	@When("^My Second Step happens$")
	public void my_Second_Step_happens() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		System.out.println("My Second Step - Bala");
	}

	@Then("^I see the result$")
	public void i_see_the_result() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		System.out.println("My Result Step - Bala");
}
}
